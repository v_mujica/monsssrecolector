package mx.bancosabadell.monsssrecolector.model;

import lombok.Data;

import java.util.Date;

@Data
public class ConteoTransaccionesDTO {
    private Date fecha;
    private String estado;
    private String descripcion;
    private Integer cantidad;
    private Integer error;
}
