package mx.bancosabadell.monsssrecolector.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class DetalleTransaccionesDTO {
	private Timestamp fecha;
	private String estado;
	private String error;
	private String razonRechazo;
	private Double monto;
}
