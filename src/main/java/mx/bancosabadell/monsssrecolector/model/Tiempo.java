package mx.bancosabadell.monsssrecolector.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class Tiempo {
    private Timestamp timestamp;
}
