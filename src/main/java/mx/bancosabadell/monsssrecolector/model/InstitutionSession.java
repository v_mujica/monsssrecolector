package mx.bancosabadell.monsssrecolector.model;

import lombok.Data;

@Data
public class InstitutionSession {
    private Integer claveInstitucion;
    private Integer conectada;
    private Integer fechaOperacion;
    private Integer folioBloqueado;
    private Integer folioDesincronizado;
    private Integer folioSolicitud;
    private Integer maxCambiosSaldoRes;
    private Integer maxPorcSaldoRes;
    private Integer numCambiosSaldoRes;
    private Double saldoInicial;
    private String ultimoMensaje;
    private Integer version;
    private Integer folioPaqueteCanc;
    private Integer folioPagoCanc;
    private Integer inicializando;
}
