package mx.bancosabadell.monsssrecolector.model;

import java.sql.Timestamp;
import lombok.Data;

@Data
public class MontSeguimientoDTO {
	private Integer id;
	private Timestamp fechaEjecucion;
	private Integer estado;
	private String descripcion;
}