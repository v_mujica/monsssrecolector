package mx.bancosabadell.monsssrecolector.utils;

import org.apache.commons.codec.binary.Base64;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Decoder {

    private static final String CADENA_VACIA = "";
    private static final String IGUAL = "=";
    private static final String INICIO = "$";
    private static final String FIN = "1";
    private static final String ALFABETO = "abcdefghijklmnopqrstuvwxyz1234567890";

    private Decoder() {
    }

    public static String decodePassword(String password) {
        String subtext = password.substring(1, (password.length() - 1));
        String realtext = subtext.substring(0, (subtext.length() / 2));
        Base64 base64 = new Base64();
        StringBuilder sb = new StringBuilder();
        byte[] strAsByteArray = realtext.getBytes();
        byte[] result = new byte[strAsByteArray.length];
        for (int i = 0; i < strAsByteArray.length; i++) result[i] = strAsByteArray[strAsByteArray.length - i - 1];
        String reversedString = new String(result);
        sb.append(reversedString);
        sb.append(Decoder.IGUAL);
        sb.append(Decoder.IGUAL);
        return new String(base64.decode(sb.toString()));
    }

    public static String encryptPassword(String text) throws NoSuchAlgorithmException {
        Random random = SecureRandom.getInstanceStrong();
        Base64 base64 = new Base64();
        int n = Decoder.ALFABETO.length();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        byte[] strAsByteArray = base64.encode(text.getBytes());
        byte[] result = new byte[strAsByteArray.length];
        for (int i = 0; i < strAsByteArray.length; i++) result[i] = strAsByteArray[strAsByteArray.length - i - 1];
        String resultado = new String(result).replace("=", Decoder.CADENA_VACIA);
        for (int i = 0; i < resultado.length(); i++) sb1.append(Decoder.ALFABETO.charAt(random.nextInt(n)));
        String password = sb1.toString();
        sb.append(Decoder.INICIO);
        sb.append(resultado);
        sb.append(password);
        sb.append(Decoder.FIN);
        return sb.toString().replace("=", Decoder.CADENA_VACIA);
    }

}