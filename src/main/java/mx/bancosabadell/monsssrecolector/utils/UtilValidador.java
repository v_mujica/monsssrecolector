package mx.bancosabadell.monsssrecolector.utils;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Se encarga de hacer validaciones m&uacute;ltiples.
 *
 * @author everis
 */
public final class UtilValidador {

    private UtilValidador() {
    }

    public static Integer getTimeShift(Timestamp lastExecutionTimestamp) {
        Date lastExecutionDate = new Date(lastExecutionTimestamp.getTime());
        Date summerScheduleEnd = null;
        Date summerScheduleStart = null;
        Boolean b = DateUtils.isBetween(lastExecutionDate, summerScheduleStart, summerScheduleEnd);
        return Boolean.TRUE.equals(b) ? -5 : -6;
    }

}
