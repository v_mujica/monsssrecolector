package mx.bancosabadell.monsssrecolector.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtils {

    private DateUtils() {
    }

    /**
     * Calcula la distancia en ms entre dos fechas
     *
     * @param from fecha inicial
     * @param to   fecha final
     * @return Long La distancia calculada.
     */
    public static Long getDistance(Date from, Date to) {
        return Math.abs(to.getTime() - from.getTime());
    }


    public static Timestamp convertDateTimeZone(Long lngDate, String timeZone) {
        TimeZone toTZ = TimeZone.getTimeZone(timeZone);
        Calendar toCal = Calendar.getInstance(toTZ);
        TimeZone fromTZ = TimeZone.getTimeZone(Calendar.getInstance().getTimeZone().getDisplayName());
        Calendar fromCal = Calendar.getInstance(fromTZ);
        fromCal.setTimeInMillis(lngDate);
        toCal.setTimeInMillis(fromCal.getTimeInMillis() + toTZ.getOffset(fromCal.getTimeInMillis()) - TimeZone.getDefault().getOffset(fromCal.getTimeInMillis()));
        return new Timestamp(toCal.getTimeInMillis());
    }

    public static Date getMaxDate(List<Date> dates) {
        Date max = null;
        if (dates != null && !dates.isEmpty()) {
            Collections.sort(dates);
            Collections.reverse(dates);
            max = dates.get(0);
        }
        return max;
    }

    public static Date getMinDate(List<Date> dates) {
        Date max = null;
        if (dates != null && !dates.isEmpty()) {
            Collections.sort(dates);
            max = dates.get(0);
        }
        return max;
    }

    public static String getDateString(String pattern, Date date) {
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    static Boolean isBetween(Date d, Date min, Date max) {
        return d.after(min) && d.before(max);
    }
}
