package mx.bancosabadell.monsssrecolector.config.partitioner;

import lombok.Data;
import lombok.extern.java.Log;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jesus Alfredo Hernandez Orozco
 */
@Log
@Data
public class ColumnRangePartitioner implements Partitioner {

    private static final String FROM = ") from ";
    private static final String WHERE_ADD = " WHERE DATEADD (hour, -6, dbo.getJavaTimeStampasDate(ts_captura)) >= '";
    private static final String AND_DATEADD = "' AND DATEADD (hour, -6, dbo.getJavaTimeStampasDate(ts_captura)) <= '";
    private JdbcTemplate jdbcTemplate;
    private String table;
    private String column;
    private Timestamp lastDate;
    private Timestamp lastToLastDate;

    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Partition a database table assuming that the data in the column specified are
     * uniformly distributed. The execution context values will have keys
     * <code>minValue</code> and <code>maxValue</code> specifying the range of
     * values to consider in each partition.
     *
     * @see Partitioner#partition(int)
     */
    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {
        log.info("SELECT MIN(" + column + FROM + table + WHERE_ADD + lastToLastDate + AND_DATEADD + lastDate + "'");
        int min = jdbcTemplate.queryForObject("SELECT MIN(" + column + FROM + table + WHERE_ADD + lastToLastDate + AND_DATEADD + lastDate + "'", Integer.class);
        int max = jdbcTemplate.queryForObject("SELECT MAX(" + column + FROM + table + WHERE_ADD + lastToLastDate + AND_DATEADD + lastDate + "'", Integer.class);
        int targetSize = (max - min) / gridSize + 1;

        Map<String, ExecutionContext> result = new HashMap<>();
        int number = 0;
        int start = min;
        int end = start + targetSize - 1;

        log.info("JAHO - Partitioner. Start: " + start + ", max: " + max);

        while (start <= max) {
            ExecutionContext value = new ExecutionContext();
            result.put("partition" + number, value);

            if (end >= max) end = max;
            value.putInt("minValue", start);
            value.putInt("maxValue", end);

            log.info("JAHO - partition" + number + ", start: " + start + ", max: " + max);
            start += targetSize;
            end += targetSize;
            number++;
        }

        return result;
    }
}