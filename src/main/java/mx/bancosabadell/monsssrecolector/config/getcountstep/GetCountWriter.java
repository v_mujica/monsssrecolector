package mx.bancosabadell.monsssrecolector.config.getcountstep;

import mx.bancosabadell.monsssrecolector.model.ConteoTransaccionesDTO;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class GetCountWriter {

    @Bean
    public JdbcBatchItemWriter<ConteoTransaccionesDTO> writerForGetCount(DataSource ds) {
        return new JdbcBatchItemWriterBuilder<ConteoTransaccionesDTO>()
                .dataSource(ds)
                .sql("insert into mont_conteo(fecha, estado, descripcion, cantidad, error) values (:fecha, :estado, :descripcion, :cantidad, :error)")
                .beanMapped()
                .build();
    }
}
