package mx.bancosabadell.monsssrecolector.config.datasources;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import mx.bancosabadell.monsssrecolector.utils.Decoder;

@Configuration
public class SpeiDataSource {

    @Value("${speidatasource.url}")
    private String url;

    @Value("${speidatasource.driver-class-name}")
    private String driverClassName;

    @Value("${speidatasource.username}")
    private String username;

    @Value("${speidatasource.password}")
    private String password;

    @Bean(name = "speidataSource")
    public DataSource getSpeiDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(Decoder.decodePassword(password));
        return dataSource;
    }
}
