package mx.bancosabadell.monsssrecolector.config.datasources;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import mx.bancosabadell.monsssrecolector.utils.Decoder;

/**
 * Configuration of the monitor data source.
 * 
 * @author Jesus Alfredo Hernandez Orozco.
 *
 */
@Configuration
public class MonitorDataSource {

    @Value("${monitordatasource.url}")
    private String url;

    @Value("${monitordatasource.driver-class-name}")
    private String driverClassName;

    @Value("${monitordatasource.username}")
    private String username;

    @Value("${monitordatasource.password}")
    private String password;

    @Primary
    @Bean(name = "monitordataSource")
    public DataSource getMonitorDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(Decoder.decodePassword(password));
        return dataSource;
    }
}
