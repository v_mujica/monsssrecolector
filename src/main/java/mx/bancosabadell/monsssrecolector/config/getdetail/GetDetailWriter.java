package mx.bancosabadell.monsssrecolector.config.getdetail;

import javax.sql.DataSource;

import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.bancosabadell.monsssrecolector.model.DetalleTransaccionesDTO;

@Configuration
public class GetDetailWriter {
    @Bean
    public JdbcBatchItemWriter<DetalleTransaccionesDTO> writerForGetDetail(DataSource ds) {
        return new JdbcBatchItemWriterBuilder<DetalleTransaccionesDTO>()
                .dataSource(ds)
                .sql("insert into mont_detalle(fecha, estado, error, razon_rechazo, monto) values (:fecha, :estado, :error, :razonRechazo, :monto)")
                .beanMapped()
                .build();
    }
}
