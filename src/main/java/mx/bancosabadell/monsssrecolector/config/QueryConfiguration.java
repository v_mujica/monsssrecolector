package mx.bancosabadell.monsssrecolector.config;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.extern.log4j.Log4j2;
import mx.bancosabadell.monsssrecolector.config.dto.QueryDTO;
import mx.bancosabadell.monsssrecolector.dao.mapper.QueryMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@Log4j2
public class QueryConfiguration {

    //@Value("${platform}")
    @Value("")
    private String platform;

    @Bean(name = "queries")
    public QueryDTO getQueries() {

        QueryDTO queryObj = null;
        try {
            queryObj = QueryMapper.getQueries(platform);
        } catch (JsonParseException e) {
            log.error("JAHO - There was an error while parsing the JSON: " + e.getCause());

        } catch (JsonMappingException e) {
            log.error("JAHO - There was an error while mapping the JSON: " + e.getCause());

        } catch (IOException e) {
            log.error("JAHO - There was an error while getting the JSON file: " + e.getCause());
        }
        return queryObj;

    }
}
