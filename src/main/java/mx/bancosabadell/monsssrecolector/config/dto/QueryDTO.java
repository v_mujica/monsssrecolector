package mx.bancosabadell.monsssrecolector.config.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class QueryDTO {
	private String queryDate;
	private String queryCountItems;
	private QueryDetailsDTO queryDetails;
}
