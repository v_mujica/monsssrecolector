package mx.bancosabadell.monsssrecolector.config.dto;

import lombok.Data;

@Data
public class QueryDetailsDTO {
	private String fields;
	private String fromClause;
	private String sortKey;
}
