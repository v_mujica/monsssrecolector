package mx.bancosabadell.monsssrecolector.config.jobs;

import lombok.extern.log4j.Log4j2;
import mx.bancosabadell.monsssrecolector.config.dao.impl.MontSeguimientoDAOImpl;
import mx.bancosabadell.monsssrecolector.config.dto.QueryDTO;
import mx.bancosabadell.monsssrecolector.config.getcountstep.GetCountWriter;
import mx.bancosabadell.monsssrecolector.config.getdetail.GetDetailWriter;
import mx.bancosabadell.monsssrecolector.config.gettimesteps.GetTimeReader;
import mx.bancosabadell.monsssrecolector.config.gettimesteps.GetTimeWriter;
import mx.bancosabadell.monsssrecolector.config.listeners.ExecutionListenerForStep;
import mx.bancosabadell.monsssrecolector.config.partitioner.ColumnRangePartitioner;
import mx.bancosabadell.monsssrecolector.model.ConteoTransaccionesDTO;
import mx.bancosabadell.monsssrecolector.model.DetalleTransaccionesDTO;
import mx.bancosabadell.monsssrecolector.model.MontSeguimientoDTO;
import mx.bancosabadell.monsssrecolector.model.Tiempo;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.logging.Logger;

@Log4j2
@Configuration
@EnableBatchProcessing
public class Batch implements BatchConfigurer {

    private static final Logger LOGGER = Logger.getLogger(Batch.class.getName());

    @Autowired
    private StepBuilderFactory stepBuilderFactory;


    @Autowired(required = true)
    @Qualifier(value = "monitordataSource")
    private DataSource monitorDataSource;

    @Autowired(required = true)
    @Qualifier(value = "queries")
    private QueryDTO queries;

    @Autowired(required = true)
    @Qualifier(value = "speidataSource")
    private DataSource speiDataSource;

    @Bean(name = "executionListenerForStep")
    public static ExecutionListenerForStep getExecutionListener() {
        return new ExecutionListenerForStep();
    }

    @Bean
    private static ExecutionContextPromotionListener promotionListener() {
        ExecutionContextPromotionListener listener = new ExecutionContextPromotionListener();
        listener.setKeys(new String[]{"executionTime"});
        return listener;
    }

    @Bean(name = "firstJob")
    public Job getFirstJob(GetDetailWriter getDetailWriter, GetCountWriter getCountWriter, GetTimeReader stepGetTime, JobBuilderFactory jobBuilderFactory) {
        return jobBuilderFactory.get("jobRecolector")
                .incrementer(new RunIdIncrementer())
                .flow(firstStep(stepGetTime))
                .next(getSecondStep(getCountWriter))
                .next(getThirdStepMaster(getDetailWriter))
                .end()
                .build();
    }

    @Bean
    private Step firstStep(GetTimeReader stepGetTime) {
        return stepBuilderFactory.get("getFirstStep")
                .<Tiempo, Tiempo>chunk(1)
                .reader(stepGetTime.jdbcReader(speiDataSource, queries))
                .writer(new GetTimeWriter(monitorDataSource))
                .listener(Batch.promotionListener())
                .build();
    }

    @Bean
    private Step getSecondStep(GetCountWriter getCountWriter) {
        return stepBuilderFactory.get("secondStepSlave")
                .<ConteoTransaccionesDTO, ConteoTransaccionesDTO>chunk(10)
                .reader(getSecondStepItemReader())
                .writer(getCountWriter.writerForGetCount(monitorDataSource))
                .allowStartIfComplete(true)
                .build();
    }

    @Bean
    private Step getThirdStepMaster(GetDetailWriter getDetailWriter) {
        return stepBuilderFactory.get("thirdStepMaster")
                .partitioner(getThirdStepSlave(getDetailWriter).getName(), partitioner())
                .step(getThirdStepSlave(getDetailWriter))
                .taskExecutor(new SimpleAsyncTaskExecutor())
                .build();
    }


    @Bean(value = "thirdStepSlave")
    private Step getThirdStepSlave(GetDetailWriter getDetailWriter) {
        return stepBuilderFactory.get("thirdStepSlave")
                .<DetalleTransaccionesDTO, DetalleTransaccionesDTO>chunk(10)
                .reader(getTihrdStepItemReader(null, null))
                .writer(getDetailWriter.writerForGetDetail(monitorDataSource))
                .allowStartIfComplete(true)
                .build();
    }

    @Bean
    @StepScope
    private ColumnRangePartitioner partitioner() {
        MontSeguimientoDAOImpl dao = new MontSeguimientoDAOImpl(monitorDataSource);
        MontSeguimientoDTO objLastSeguimiento = dao.getLastSeguimiento();
        MontSeguimientoDTO objLastToLastSeguimiento = dao.getLastToLastSeguimiento();

        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();

        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(speiDataSource);
        columnRangePartitioner.setLastDate(objLastSeguimiento.getFechaEjecucion());
        columnRangePartitioner.setLastToLastDate(objLastToLastSeguimiento.getFechaEjecucion());
        columnRangePartitioner.setTable("sp_ord_pago_rec");
        return columnRangePartitioner;
    }

    @Bean
    @StepScope
    private ItemReader<ConteoTransaccionesDTO> getSecondStepItemReader() {

        MontSeguimientoDAOImpl dao = new MontSeguimientoDAOImpl(monitorDataSource);
        MontSeguimientoDTO objLastSeguimiento = dao.getLastSeguimiento();
        MontSeguimientoDTO objLastToLastSeguimiento = dao.getLastToLastSeguimiento();

        String finalQuery = queries.getQueryCountItems().replace("1p", objLastToLastSeguimiento.getFechaEjecucion().toString()).replace("2p",
                objLastSeguimiento.getFechaEjecucion().toString());
        log.info(finalQuery);

        JdbcCursorItemReader<ConteoTransaccionesDTO> itemReader = new JdbcCursorItemReader<>();
        itemReader.setDataSource(speiDataSource);
        itemReader.setSql(finalQuery);
        itemReader.setRowMapper(new BeanPropertyRowMapper<>(ConteoTransaccionesDTO.class));
        itemReader.open(new ExecutionContext());
        return itemReader;
    }

    @Bean
    @StepScope
    private ItemReader<DetalleTransaccionesDTO> getTihrdStepItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue) {

        SqlPagingQueryProviderFactoryBean queryProvider = new SqlPagingQueryProviderFactoryBean();
        queryProvider.setDataSource(speiDataSource);
        queryProvider.setSelectClause(queries.getQueryDetails().getFields());
        queryProvider.setFromClause(queries.getQueryDetails().getFromClause());
        queryProvider.setWhereClause("where " + queries.getQueryDetails().getSortKey() + " >= " + minValue + " and " + queries.getQueryDetails().getSortKey() + " <= " + maxValue);
        queryProvider.setSortKey(queries.getQueryDetails().getSortKey());

        JdbcPagingItemReader<DetalleTransaccionesDTO> reader = new JdbcPagingItemReader<>();
        reader.setDataSource(speiDataSource);

        PagingQueryProvider pagingQueryProvider = null;
        try {
            pagingQueryProvider = queryProvider.getObject();
            reader.setQueryProvider(pagingQueryProvider);
            reader.setPageSize(10);
            reader.setRowMapper(new BeanPropertyRowMapper<>(DetalleTransaccionesDTO.class));
        } catch (Exception e) {
            Batch.LOGGER.severe(e.getMessage());
        }
        return reader;
    }

    @Override
    public JobRepository getJobRepository() throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(monitorDataSource);
        factory.setTransactionManager(getTransactionManager());
        factory.setIsolationLevelForCreate("ISOLATION_SERIALIZABLE");
        factory.setTablePrefix("BATCH_");
        factory.setMaxVarCharLength(1000);
        return factory.getObject();
    }

    @Override
    public PlatformTransactionManager getTransactionManager() {
        return new DataSourceTransactionManager(monitorDataSource);
    }

    @Override
    public JobLauncher getJobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(getJobRepository());
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        jobLauncher.afterPropertiesSet();
        return jobLauncher;
    }

    @Override
    public JobExplorer getJobExplorer() throws Exception {
        return null;
    }
}
