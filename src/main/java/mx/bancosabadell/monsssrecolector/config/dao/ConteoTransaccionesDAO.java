package mx.bancosabadell.monsssrecolector.config.dao;

import mx.bancosabadell.monsssrecolector.model.ConteoTransaccionesDTO;

import java.sql.Timestamp;
import java.util.List;

public interface ConteoTransaccionesDAO {
    List<ConteoTransaccionesDTO> getGroupedTransactions(Timestamp dateOfLastExecution, Timestamp dateOfExecution);
}
