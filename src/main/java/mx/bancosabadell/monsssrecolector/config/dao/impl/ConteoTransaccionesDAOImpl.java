package mx.bancosabadell.monsssrecolector.config.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import lombok.extern.java.Log;
import mx.bancosabadell.monsssrecolector.config.dao.ConteoTransaccionesDAO;
import mx.bancosabadell.monsssrecolector.dao.mapper.ConteoTransaccionesRowMapper;
import mx.bancosabadell.monsssrecolector.model.ConteoTransaccionesDTO;

@Log
public class ConteoTransaccionesDAOImpl implements ConteoTransaccionesDAO {

    private JdbcTemplate jdbcTemplate;

    public ConteoTransaccionesDAOImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final String QUERY = "SELECT SYSDATETIME() as fecha, estado, error, razon_rechazo, COUNT(*) as cantidad \r\n" + 
    		"FROM sp_ord_pago_rec \r\n" + 
    		"WHERE DATEADD (hour, -6, dbo.getJavaTimeStampasDate(ts_captura)) >= '1p' \r\n" + 
    		"AND DATEADD (hour, -6, dbo.getJavaTimeStampasDate(ts_captura)) <= '2p' \r\n" + 
    		"GROUP BY estado, error, razon_rechazo";
    
    
    /**
     * Gets the number of transactions grouped by state.
     *
     * @param dateOfLastExecution Date of last batch execution.
     * @return List with the the number of transactions grouped by state.
     */
    @Override
    public List<ConteoTransaccionesDTO> getGroupedTransactions(Timestamp dateOfLastExecution, Timestamp dateOfExecution) {
    	log.info("Entra a getGroupedTransactions()");
        List<ConteoTransaccionesDTO> list = this.jdbcTemplate.query(QUERY.replace("1p", dateOfLastExecution.toString()).replace("2p", dateOfExecution.toString()), new ConteoTransaccionesRowMapper());
        
    	log.info("getGroupedTransactions - Tamanio de la lista obtenida: " + list);
        return list;
    }
}
