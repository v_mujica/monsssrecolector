package mx.bancosabadell.monsssrecolector.config.dao.impl;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import mx.bancosabadell.monsssrecolector.config.dao.MontSeguimientoDAO;
import mx.bancosabadell.monsssrecolector.model.MontSeguimientoDTO;

public class MontSeguimientoDAOImpl implements MontSeguimientoDAO {

	private JdbcTemplate jdbcTemplate;

    public MontSeguimientoDAOImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
	
	@Override
	public MontSeguimientoDTO getLastToLastSeguimiento() {
		return this.jdbcTemplate.queryForObject("SELECT * FROM mont_seguimiento WHERE id= ((select max(id) from mont_seguimiento)-1)", 
			new BeanPropertyRowMapper<MontSeguimientoDTO>(MontSeguimientoDTO.class));
	}
	
	@Override
	public MontSeguimientoDTO getLastSeguimiento() {
		return this.jdbcTemplate.queryForObject("SELECT TOP 1 * FROM mont_seguimiento ORDER BY ID DESC", 
			new BeanPropertyRowMapper<MontSeguimientoDTO>(MontSeguimientoDTO.class));
	}

}