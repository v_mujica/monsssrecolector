package mx.bancosabadell.monsssrecolector.config.dao;

import mx.bancosabadell.monsssrecolector.model.MontSeguimientoDTO;

public interface MontSeguimientoDAO {

	MontSeguimientoDTO getLastToLastSeguimiento();

	MontSeguimientoDTO getLastSeguimiento();
}
