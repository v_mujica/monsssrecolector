package mx.bancosabadell.monsssrecolector.config.listeners;

import lombok.extern.java.Log;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;

@Log
public class ExecutionListenerForStep implements StepExecutionListener {


    @Override
    public void beforeStep(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        ExecutionContext jobContext = jobExecution.getExecutionContext();
        log.info("BeforeStep - jobContext.get(\"executionTime\"): " + jobContext.get("executionTime"));
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.info("ExecutionListenerForStep afterStep");
        return null;
    }

}
