package mx.bancosabadell.monsssrecolector.config.gettimesteps;

import lombok.extern.java.Log;
import mx.bancosabadell.monsssrecolector.model.Tiempo;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.util.List;

@Log
public class GetTimeWriter implements ItemWriter<Tiempo> {
    private JdbcBatchItemWriter<Tiempo> itemWriter;
    private StepExecution stepExecution;

    public GetTimeWriter(DataSource dataSource) {
        JdbcBatchItemWriter<Tiempo> jdbcBatchItemWriter = new JdbcBatchItemWriter<>();
        jdbcBatchItemWriter.setDataSource(dataSource);
        jdbcBatchItemWriter.setSql("insert into mont_seguimiento (fechaEjecucion, estado, descripcion) values (:timestamp, 1, null)");
        jdbcBatchItemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        jdbcBatchItemWriter.afterPropertiesSet();
        itemWriter = jdbcBatchItemWriter;
    }

    @Override
    public void write(List<? extends Tiempo> items) throws Exception {
        ExecutionContext stepContext = stepExecution.getExecutionContext();
        stepContext.put("executionTime", items.get(0).getTimestamp());
        log.info("JAHO - Last Execution: " + items.get(0).getTimestamp());
        itemWriter.write(items);
    }

    @BeforeStep
    public void saveStepExecution(StepExecution stepExecution) {
        this.stepExecution = stepExecution;
    }
}