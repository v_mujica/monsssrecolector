package mx.bancosabadell.monsssrecolector.config.gettimesteps;

import mx.bancosabadell.monsssrecolector.config.dto.QueryDTO;
import mx.bancosabadell.monsssrecolector.dao.mapper.TiempoMapper;
import mx.bancosabadell.monsssrecolector.model.Tiempo;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class GetTimeReader {

    @Bean
    public ItemReader<Tiempo> jdbcReader(DataSource dataSource, QueryDTO queries) {
        return new JdbcCursorItemReaderBuilder<Tiempo>()
                .dataSource(dataSource)
                .name("sssReader")
                .sql(queries.getQueryDate())
                .rowMapper(new TiempoMapper())
                .build();
    }
}
