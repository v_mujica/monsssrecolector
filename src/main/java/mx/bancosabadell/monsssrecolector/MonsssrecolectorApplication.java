package mx.bancosabadell.monsssrecolector;

import lombok.extern.log4j.Log4j2;
import org.jasypt.util.text.AES256TextEncryptor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@Log4j2
@ComponentScan("mx.bancosabadell.monsssrecolector.config")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class MonsssrecolectorApplication implements CommandLineRunner {

    @Autowired
    @Qualifier(value = "jobLauncher")
    private
    JobLauncher jobLauncher;

    @Autowired
    @Qualifier(value = "firstJob")
    private
    Job job;

    public static void main(String[] args) {
        SpringApplication.run(MonsssrecolectorApplication.class);
    }

    @Override
    public void run(String... args) throws Exception {

        final String password = "password";
        AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
        textEncryptor.setPassword(password);

        String myEncryptedText = textEncryptor.encrypt(password);
        String plainText = textEncryptor.decrypt(myEncryptedText);

        log.info("JAHO - Password en texto planto: " + password);
        log.info("JAHO - Password encriptado: " + myEncryptedText);
        log.info("JAHO - Password des encriptado: " + plainText);

        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(job, params);
    }

}
