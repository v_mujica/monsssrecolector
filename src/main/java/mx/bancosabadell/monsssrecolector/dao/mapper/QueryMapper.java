package mx.bancosabadell.monsssrecolector.dao.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import mx.bancosabadell.monsssrecolector.config.dto.QueryDTO;

import java.io.File;
import java.io.IOException;

public final class QueryMapper {

    private static final String BASE_PATH = QueryMapper.getPath();

    private QueryMapper() {
    }

    private static String getPath() {
        String home = System.getProperty("user.home");
        String formatted = home.replace("\\", "/");
        return String.format("%s/Documents/", formatted);
    }

    public static QueryDTO getQueries(String platform) throws IOException {
        StringBuilder sb = new StringBuilder();
        ObjectMapper objectMapper = new ObjectMapper();
        sb.append(QueryMapper.BASE_PATH).append(platform).append("/monsssrecolector.json");
        return objectMapper.readValue(new File(sb.toString()), QueryDTO.class);
    }
}
