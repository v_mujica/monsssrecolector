package mx.bancosabadell.monsssrecolector.dao.mapper;

import mx.bancosabadell.monsssrecolector.model.Tiempo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TiempoMapper implements RowMapper<Tiempo> {
    @Override
    public Tiempo mapRow(ResultSet resultSet, int i) throws SQLException {
        Tiempo objeto = new Tiempo();
        objeto.setTimestamp(resultSet.getTimestamp(1));
        return objeto;
    }
}
