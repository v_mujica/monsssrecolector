package mx.bancosabadell.monsssrecolector.dao.mapper;

import mx.bancosabadell.monsssrecolector.model.ConteoTransaccionesDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConteoTransaccionesRowMapper implements RowMapper<ConteoTransaccionesDTO> {
    @Override
    public ConteoTransaccionesDTO mapRow(ResultSet rs, int i) throws SQLException {
        ConteoTransaccionesDTO objeto = new ConteoTransaccionesDTO();
        objeto.setFecha(rs.getDate("fecha"));
        objeto.setEstado(rs.getString("estado"));
        objeto.setEstado(rs.getString("error"));
        objeto.setDescripcion(rs.getString("razon_rechazo"));
        return objeto;
    }
}